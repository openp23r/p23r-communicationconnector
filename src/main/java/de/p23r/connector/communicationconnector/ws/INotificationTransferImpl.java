package de.p23r.connector.communicationconnector.ws;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.atomic.AtomicInteger;

import javax.jws.WebService;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.soap.MTOM;

import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3._2000._09.xmldsig.SignatureType;

import de.p23r.leitstelle.ns.p23r.common1_0.NameValuePair;
import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.inotificationtransfer1_2.INotificationTransfer;
import de.p23r.leitstelle.ns.p23r.inotificationtransfer1_2.types.Channel;
import de.p23r.leitstelle.ns.p23r.inotificationtransfer1_2.types.Payload;
import de.p23r.leitstelle.ns.p23r.inotificationtransfer1_2.types.SendProtocol;

/**
 * The Class INotificationTransferImpl. Implementation class for the INotificationTransfer interface (sendNotification).
 * It simply stores the notification in <code><user.home>/Notifications/<rulename>/</code>. If possible it also tries to apply any XFO
 * presentation scripts and stores the result to the same location as the original notification.
 * 
 * @author sim
 * 
 */
@WebService(serviceName = "INotificationTransfer", endpointInterface = "de.p23r.leitstelle.ns.p23r.inotificationtransfer1_2.INotificationTransfer", targetNamespace = "http://leitstelle.p23r.de/NS/P23R/INotificationTransfer1-2")
@MTOM(enabled = true)
public class INotificationTransferImpl implements INotificationTransfer {

	private static AtomicInteger count = new AtomicInteger();

	private final Logger log = LoggerFactory.getLogger(INotificationTransferImpl.class);

	/* (non-Javadoc)
	 * @see de.p23r.leitstelle.ns.p23r.inotificationtransfer1_0.INotificationTransfer#sendNotification(de.p23r.leitstelle.ns.p23r.inotificationtransfer1_0.types.Payload, de.p23r.leitstelle.ns.p23r.inotificationtransfer1_0.types.Channel, java.lang.String)
	 */
	@Override
	public SendProtocol sendNotification(Payload payload, Channel channel, String ruleId, String ruleName, String notificationId, String transmissionId) throws P23RAppFault_Exception {
		if (log.isDebugEnabled()) {
			log.debug("-> sendNotifcation()");
			log.debug("-  channel.type: {}", channel.getType());
			log.debug("-  channel.timeoutAt: {}", channel.getTimeoutAt());
			log.debug("-  channel.representationScript: {}", channel.getRepresentationScript());
			if (channel.getParameter() != null) {
				for (NameValuePair parameter : channel.getParameter()) {
					log.debug("-  channel.parameter: {} - {}", parameter.getName(), parameter.getValue());

				}
			}
			log.debug("-  ruleName: {}", ruleName);
		}

		InputStream notification = null;
		try {
			notification = payload.getNotification().getContent().getInputStream();
		} catch (IOException e) {
			log.error("get payload stream", e);
		}

		SignatureType contentSignature = payload.getSignedContent();
		if (contentSignature != null) {
		}
		SignatureType coreSignature = payload.getSignedCore();
		if (coreSignature != null) {
		}

		String filename = saveNotification(notification, ruleName);

		SendProtocol prot = new SendProtocol();
		prot.setTransmissionSuccessful(true);
		try {
			prot.setTransmissionTime(DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar()));
		} catch (DatatypeConfigurationException e) {
			log.warn("setting current transmission time", e);
		}

		String script = channel.getRepresentationScript();
		sendNotification(script, notification, filename, prot);

		log.debug("<- returning sent protocol successfully");
		return prot;
	}

	private void sendNotification(String script, InputStream notification, String filename, SendProtocol prot) throws P23RAppFault_Exception {
		if (script != null && !script.isEmpty()) {
			SAXBuilder builder = new SAXBuilder();
			try {
				Document doc = builder.build(new ByteArrayInputStream(script.getBytes(Charset.forName("UTF-8"))));
				if (hasXSLNamespace(doc) && !transformFo(notification, script, filename)) {
					prot.setTransmissionSuccessful(false);
					prot.setInformationText("fop transformation failed.");
				} else if (!transformXslt(notification, script, filename)) {
					prot.setTransmissionSuccessful(false);
					prot.setInformationText("xslt transformation failed.");
				}
			} catch (JDOMException e) {
				throw new P23RAppFault_Exception("failed to send notification.", e);
			} catch (IOException e) {
				throw new P23RAppFault_Exception("failed to build script document.", e);
			}
		}
	}

	private boolean hasXSLNamespace(Document doc) {
		return doc.getRootElement().getNamespaceURI() != null && "http://www.w3.org/1999/XSL/Format".equals(doc.getRootElement().getNamespaceURI());
	}

	private String saveNotification(InputStream notification, String rulename) {
		String userHome = System.getProperty("user.home");

		File storeDir = new File(userHome + File.separator + "Notifications" + File.separator + rulename);
		if (!storeDir.mkdirs()) {
			log.error("failed to create store directory.");
		}

		String filename = count.incrementAndGet() + "_" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		File xmlFile = new File(storeDir, filename + ".xml");

		PrintStream out = null;
		try {
			out = new PrintStream(xmlFile, "UTF-8");
			out.print(notification);
		} catch (FileNotFoundException e) {
			log.error("saving notification failed because file " + xmlFile.getAbsolutePath() + " could not be found.", e);
		} catch (UnsupportedEncodingException e) {
			log.error("encoding utf-8 is not supported", e);
		} finally {
			if (out != null) {
				out.close();
			}
		}
		return xmlFile.getAbsolutePath();
	}

	private boolean transformFo(InputStream xmlData, String xslData, String filename) {
		FileOutputStream pdf = null;
		try {
			pdf = new FileOutputStream(new File(filename.replace(".xml", ".pdf")));
			Fop fop = FopFactory.newInstance().newFop(MimeConstants.MIME_PDF, pdf);
			Result sax = new SAXResult(fop.getDefaultHandler());
			Transformer transformer = TransformerFactory.newInstance().newTransformer(
					new StreamSource(new ByteArrayInputStream(xslData.getBytes(Charset.forName("UTF-8")))));
			transformer.transform(new StreamSource(xmlData), sax);
			return true;
		} catch (FileNotFoundException e) {
			log.error("file " + filename.replace(".xml", ".pdf") + " not found.", e);
		} catch (FOPException e) {
			log.error("FOP construction failed.", e);
		} catch (TransformerConfigurationException e) {
			log.error("Transformer construction failed.", e);
		} catch (TransformerException e) {
			log.error("transformation failed.", e);
		} finally {
			if (pdf != null) {
				try {
					pdf.close();
				} catch (IOException e) {
					log.error("failed to close file output stream.", e);
				}
			}
		}
		return false;
	}

	private boolean transformXslt(InputStream xmlData, String xslData, String filename) {
		FileOutputStream file = null;
		try {
			file = new FileOutputStream(new File(filename.replace(".xml", "")));
			Transformer transformer = TransformerFactory.newInstance().newTransformer(
					new StreamSource(new ByteArrayInputStream(xslData.getBytes(Charset.forName("UTF-8")))));
			transformer.transform(new StreamSource(xmlData), new StreamResult(file));
			return true;
		} catch (FileNotFoundException e) {
			log.error("file " + filename + " not found.", e);
		} catch (TransformerConfigurationException e) {
			log.error("Transformer construction failed.", e);
		} catch (TransformerException e) {
			log.error("transformation failed.", e);
		} finally {
			if (file != null) {
				try {
					file.close();
				} catch (IOException e) {
					log.error("failed to close file output stream.", e);
				}
			}
		}
		return false;
	}

}
