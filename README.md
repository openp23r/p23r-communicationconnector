# P23R Communication Connector

## Build and install the connector

Prerequisites:

* JDK 1.8 or newer
* WildFly 8.1.0.Final

Then, to build and deploy from source:

 1. Clone the git repository:
    ```
    $> git clone https://gitlab.com/openp23r/p23r-communicationconnector.git
    ```

 2. Build and install the connector:
    
    ```
    $> mvn package wildfly:deploy
    ```
It is necessary that the server (WildFly) is running. Otherwise the deployment will fail.

## Legal issues

This project is part of the openP23R initiative. All related projects are listed at https://gitlab.com/openp23r/openp23r .

The legal conditions see LICENSE file.

The project is maintained by P23R-Team (a) Fraunhofer FOKUS

By submitting a "pull request" or otherwise contributing to this repository, you agree to license your contribution under the license mentioned above.